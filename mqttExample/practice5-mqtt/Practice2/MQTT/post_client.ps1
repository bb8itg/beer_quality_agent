function post_message($topic, $message)
{
	Write-Output "TOPIC: $topic"
    Write-Output "MESSAGE: $message"
	mosquitto_pub -h localhost -p 1883 -t "$topic" -m "$message"
}

Write-Output "This script will post messages in the local MQTT broker."

Start-Sleep 1
post_message "/test" "DATA_test"
Start-Sleep 1
post_message "/test/a" "DATA_test-a"
Start-Sleep 1
post_message "/test/a/a" "DATA_test-a-a"
Start-Sleep 1
post_message "/test/a/b" "DATA_test-a-b"
Start-Sleep 1
post_message "/test/b/a" "DATA_test-b-a"
Start-Sleep 1
post_message "/test/b" "DATA_test-b"
Start-Sleep 1
post_message "/test/b/b" "DATA_test-b-b"
Start-Sleep 1
post_message "/test/x/y/a" "DATA_test-x-y-a"