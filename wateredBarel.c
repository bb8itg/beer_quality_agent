//IMPORTANT TO UNCOMMENT UNDEF IN GOLBALS FOR SIGNALS
#include "barel/barel.h"
#include "bar/bar.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>


int main(){
    Barel* testBarel = createBarel(4., 16., 10);
    int input = 0;
    while(1){
        printf("\e[1;1H\e[2J");
        printf("1) add water\n2) send data\n");
        scanf("%d", &input);
        if(input == 1){
            testBarel->pHsensor += 0.5;
        } 
        else if(input == 2){
            printf("%f\n", readSensorData(testBarel));
            sleep(1);
        }

    }
    return 0;
}