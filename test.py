import time
from bar.bar import Bar
from bar.global_params import *

the_bar = Bar('BIG MAC', FIREBASE_URL, FIREBASE_NODE)

the_bar.add_barrel('11')
the_bar.add_barrel('22')
the_bar.add_barrel('33')

the_bar.update_barrel('11', 'ph', 4.5)
the_bar.update_barrel('22', 'ph', 4.5)
the_bar.update_barrel('33', 'ph', 4.5)

the_bar.update_barrel('11', 'tmp', 24.5)
the_bar.update_barrel('22', 'tmp', 26.5)
the_bar.update_barrel('33', 'tmp', 22.5)

the_bar.firebase_post()