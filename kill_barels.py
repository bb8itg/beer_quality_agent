import psutil
import sys
def get_barels_pid_list():
    pid_list = []
    ppid_list = []
    for proc in psutil.process_iter(['pid', 'name']):
        if 'barel.out' in proc.info['name']:
            pid = proc.pid
            ppid = psutil.Process(pid).ppid()
            pid_list.append(pid)
            ppid_list.append(ppid)
    return pid_list, ppid_list

def kill_process_tree(pid):
    try:
        parent = psutil.Process(pid)
        children = parent.children(recursive=True)
        for child in children:
            child.kill()
        psutil.wait_procs(children, timeout=5)
        parent.kill()
        parent.wait(5)
        print("Process tree with PID {} has been terminated.".format(pid))
    except psutil.NoSuchProcess:
        print("Process with PID {} does not exist.".format(pid))

if __name__ == '__main__':
    # PID = int(sys.argv[1])
    pid_list, ppid_list = get_barels_pid_list()
    for ppid in ppid_list:
        if ppid in pid_list:
            kill_process_tree(ppid)