#include "barel/barel.h"
#include "bar/bar.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>

//gcc test.c bar/*.c barel/*.c -lmosquitto

int main(){
    srand(time(NULL));
    int barelNum = 3;
    Barel* testBarel[barelNum];
    Bar* bar = createBar();
    pid_t p = fork();

    if(p != 0){
        //PARENT THREAD
        startBar(bar);
    } else{
        for(int i = 0; i < barelNum; i++){
            testBarel[i] = createBarel(4., 16.);
            sleep(1);
        }
        for(int i = 0; i < 20; i++){
            printf("%f\n", readSensorData(testBarel[ rand() % barelNum]));
            sleep(1);
        }
    }

    return 0;
}