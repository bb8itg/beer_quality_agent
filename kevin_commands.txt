wsl----
cd /mnt/d/REPOS/beer_quality_agent

------- MQTT -------------
mosquitto -p 1884 -v
gcc mosquitto_pub.c -lmosquitto

RAS ip
192.168.137.181

gcc barInit.c barel/*.c bar/*.c -lmosquitto -o bar.out
gcc barelsInit.c barel/*.c bar/*.c -lmosquitto -o barel.out
gcc wateredBarel.c barel/*.c bar/*.c -lmosquitto -o wateredbarel.out

killall -u amibirag
mosquitto -c mosquitto.conf -v