# importing libraries
import numpy as np
import time
import matplotlib.pyplot as plt

class website:
    def __init__(self,key,value):
        plt.ion()
        self.numberOfPlot=len(value)
        self.figure ,self.ax= plt.subplots(self.numberOfPlot, len(value[0])-1, figsize=(10, 8))
        if self.numberOfPlot == 1:
            self.ax = [self.ax]
        self.lines={}
        self.linesValues={}
        i=0
        k=0
        while k < len(self.ax):
            keys=list(value[k].keys())[1:]
            vectors={}
            i=0
            print(keys)
            for key in keys:
                vector = np.zeros(99)
                vector = np.append(vector,value[k][key])
                vectors.update({key:vector})
                self.lines.update({value[k]['id']+key:self.ax[k][i].plot(vector, label=key)[0]})
                self.ax[k][i].set_title(key, fontsize=20)
                self.ax[k][i].set_xlabel("Time")
                self.ax[k][i].set_ylabel(key)
                self.ax[k][i].set_ylim([-10,20])
                i=i+1
            self.linesValues.update({value[k]['id']:vectors})
            k=k+1
            
            
    def recreate(self,value):
        plt.ion()
        self.numberOfPlot=len(value)
        self.figure ,self.ax= plt.subplots(self.numberOfPlot, len(value[0])-1, figsize=(10, 8))
        i=0
        k=0
        while k < len(self.ax):
            keys=list(value[i].keys())[1:]
            vectors={}
            i=0
            for key in keys:
                if(value[i]["id"] in list(self.linesValues.keys)):
                    self.ax[k][i].set_title(key, fontsize=20)
                    self.ax[k][i].set_xlabel("Time")
                    self.ax[k][i].set_ylabel(key)
                    k=k+1
                    continue
                vector = np.zeros(99)
                vector = np.append(vector,value[i][key])
                vectors.update({key:vector})
                self.lines.update({value[k]['id']+key:self.ax[k][i].plot(vector, label=key)[0]})
                self.ax[k][i].set_title(key, fontsize=20)
                self.ax[k][i].set_xlabel("Time")
                self.ax[k][i].set_ylabel(key)
                i=i+1
            self.linesValues.update({value[k]['id']:vectors})
            k=k+1
            
            
    def updatePlot(self,values):
        #if(len(values)> len(self.linesValues)):
            #self.recreate(values)
        for value in values:
            keys=list(value.keys())[1:]
            for key in keys:
                # Append the new value
                self.linesValues[value['id']][key] = np.append(self.linesValues[value['id']][key], value[key])
                # Remove the first element
                self.linesValues[value['id']][key] = self.linesValues[value['id']][key][1:]
 
                self.lines[value['id']+key].set_ydata(self.linesValues[value['id']][key])
    def draw(self):
        # drawing updated values
        self.figure.canvas.draw()
        # This will run the GUI event loop until all UI events currently waiting have been processed
        self.figure.canvas.flush_events()

