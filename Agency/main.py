import subprocess
import threading
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from website import website
import json
import time
#from website import website

cred = credentials.Certificate("firebass-7a161-firebase-adminsdk-ueyga-e4e4974811.json")
# firebase_admin.initialize_app(cred)
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://firebass-7a161-default-rtdb.europe-west1.firebasedatabase.app'
    })

# Reference to the database service
ref = db.reference('bars')  # Replace with your database path
people = db.reference('people')  # Replace with your database path
warning = db.reference('warning') 


with open('check.json') as json_data:
    check_conf = json.load(json_data)
    json_data.close()
    #print(check_conf)


activeBars=set()
websites={}
# Function to read messages
# Parse messages by bars and give each bars to the website 
def read_messages():
    messages = ref.get()  # Retrieve all messages
    if messages is not None:
        for bars,message_values in messages.items():
            if(type(message_values) is str):
                message_values=json.loads(message_values)
                check_values(message_values)
                if message_values:
                    messages_changed = {message_values['bar_id']: message_values['barrels']}
                    for key, value in messages_changed.items():
                        if(key in activeBars):
                            websites.get(key).updatePlot(value)
                        else:
                            tempsite=website(key,value)
                            websites.update({key:tempsite})
                            activeBars.add(key)
                        # Print each message key-value pair
                        # delete message after read
                        ref.child(bars).delete()

# Check temprature and ph values

def check_values(message_values):
    if message_values:
        messages_changed = {message_values['bar_id']: message_values['barrels']} 
        for key, values in messages_changed.items():
            for value in values:
                if value['ph'] < check_conf['ph_lower'] or value['ph'] >check_conf['ph_higher'] :
                    warning.set({key:'PH_Warning'})
                if value['tmp'] >check_conf['tmp_higher']:
                    warning.set({key:'Temp_Warning'})

while True:
    print("Check server")
    read_messages()
    for _,website in websites.items():
        website.draw()
    time.sleep(5)
