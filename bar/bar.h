#include "../barel/barel.h"
#include "../globals.h"

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>
#include <stdio.h>
#include <unistd.h>


typedef struct BarelArray{
    Barel* Barel;
    int current;
    int len;
} BarelArray;

typedef struct Bar{
    //float deviancy; // indicator if it will pour extra water in the beer or not
    BarelArray* barelArray;
} Bar;


Bar* createBar();
int startBar(Bar*);

// ======================== MQTT CALLS ======================
void my_message_callback(struct mosquitto *mosq, void *userdata, const struct mosquitto_message *message);
void my_connect_callback(struct mosquitto *mosq, void *userdata, int result);
void my_subscribe_callback(struct mosquitto *mosq, void *userdata, int mid, int qos_count, const int *granted_qos);