import paho.mqtt.client as mqtt
from firebase import firebase
import json
import time
# from global_params import *
import sys

BROKER_IP = "10.211.55.2"
PORT = 1884
MQTT_TOPICS = ["/barrels/#"]
FIREBASE_URL = "https://firebass-7a161-default-rtdb.europe-west1.firebasedatabase.app/"
FIREBASE_NODE = "bars"
class Barrel:

    def __init__(self, id) -> None:
        self.__id = id
        self.__ph = -1
        self.__temp = -1
    
    def get_id(self) -> str:
        return self.__id
    
    def update(self, data_type, value) -> None:
        if data_type == 'ph':
            self.__ph = value
        elif data_type == 'tmp':
            self.__temp = value
        self.__last_update_time = time.time()
    
    def get_last_update(self) -> float:
        return self.__last_update_time
    
    def get_ph(self) -> float:
        return self.__ph
    
    def get_temp(self) -> float:
        return self.__temp
    
class Bar:
    def __init__(self, id, firebase_url, firebase_node) -> None:
        self.__id = id
        self.__barrels = dict()
        self.__check_interval = 10
        self.__last_check = time.time()
        self.__firebase = firebase.FirebaseApplication(firebase_url, None)
        self.__firebase_node = firebase_node
    
    def connect_mqtt(self, broker_ip, port, topics) -> str:
        # self.__firebase = firebase.FirebaseApplication(firebase_url, None)
        # def on_connect(client, userdata, flags, rc):
        #     if rc == 0:
        #         print(f"Connected to broker: {broker_ip}")
        #         for topic in topics:
        #             client.subscribe(topic)
        #     else:
        #         print("Connection failed")
                
        def on_message(client, userdata, message):
            print(message.topic)
            print("Received message:", str(message.payload.decode("utf-8")))
            self.parse_mqtt(message)
            self.firebase_post()
        
        self.__client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
        # self.__client.on_connect = on_connect
        self.__client.on_message = on_message
        try:
            self.__client.connect(broker_ip, port)
            print('connected')
        except ConnectionRefusedError:
            print('Connection error')
            return
        
        for topic in topics:
            self.__client.subscribe(topic)
        self.__client.loop_start()
        try:
            while True:
                pass
                # if time.time() - self.__last_check >= self.__check_interval:
                #     self.check_barrels()
        except KeyboardInterrupt:
            print("Exiting")
            self.__client.disconnect()
            self.__client.loop_stop()    
    
    def get_id(self) -> str:
        return self.__id
    
    def add_barrel(self, barrel_id) -> None:
        self.__barrels[barrel_id] = Barrel(barrel_id)
    
    def drop_barrel(self, barrel_id) -> Barrel:
        return self.__barrels.pop(barrel_id)
    
    def parse_mqtt(self, message) -> None:
        topic = message.topic.split('/')
        id = topic[-1]
        # data_type = topic[2]
        if not self.__barrels.get(id):
            self.add_barrel(id)
        data = message.payload.decode("utf-8").split(' ')
        for i in range(0,len(data),2):
            data_type = data[i]
            value = float(data[i+1])
            self.update_barrel(id, data_type, value)
    
    def update_barrel(self, barrel_id, data_type, value) -> None:
        self.__barrels[barrel_id].update(data_type, value) 
    
    def check_barrels(self) -> None:
        curr = time.time()
        for key,val in self.__barrels.items():
            if curr - val.get_last_update() > self.__check_interval:
                self.drop_barrel(key)
        
    def firebase_post(self) -> None:
        barrels_list = []
        for _,barrel in self.__barrels.items():
            barrels_list.append({
                "id": barrel.get_id(),
                "ph": barrel.get_ph(),
                "tmp": barrel.get_temp()
            })
        data = {
            "bar_id": self.__id,
            "barrels": barrels_list,
        }
        json_data = json.dumps(data, indent=4)
        ret = self.__firebase.post(self.__firebase_node, json_data)
        
    def firebase_sub(self) -> None:
        pass
    
if __name__ == "__main__":
    
    bar_name = sys.argv[1]
    
    the_bar = Bar(bar_name, FIREBASE_URL, FIREBASE_NODE)
    the_bar.connect_mqtt(BROKER_IP, PORT, MQTT_TOPICS)