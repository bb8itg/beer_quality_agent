from firebase import firebase
# import firebase_admin
# from firebase_admin import db
import json


# firebase_admin.initialize_app(None, {
#     'databaseURL': 'https://erts-2024-default-rtdb.europe-west1.firebasedatabase.app/'
# })

myfirebase = firebase.FirebaseApplication("https://firebass-7a161-default-rtdb.europe-west1.firebasedatabase.app/",None)

data = {
    "bar_id": "01",
    "barrels": [{"barrel_id": "barrel01", "ph":4.5, "volume":0.5},
                {"barrel_id": "barrel02", "ph":4.1, "volume":0.8}],
}

# Convert the dictionary to JSON format
json_data = json.dumps(data, indent=4)

# Print the JSON data
# print(json_data)
# try:
#     ret = firebase.post('bars', json_data)
# except Warning:
#     # print('warning')
#     pass
# print(ret)
ret = myfirebase.post('bars', json_data)
# ret = firebase.get('/messages')

# # Reference to your database
# ref = db.reference('/messages')

# # Retrieve data
# data = ref.get()

# # Print the retrieved data
# print(data)