import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

cred = credentials.Certificate("firebass-7a161-firebase-adminsdk-ueyga-e4e4974811.json")
# firebase_admin.initialize_app(cred)
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://firebass-7a161-default-rtdb.europe-west1.firebasedatabase.app'
})


# Reference to the database service
ref = db.reference('bars')  # Replace with your database path

# Function to read messages
def read_messages(event):
    # ref = db.reference('bars')  # Reference to the 'messages' node in your database
    messages = ref.get()  # Retrieve all messages
    if messages:
        for key, value in messages.items():
            print(key, value)  # Print each message key-value pair
            # delete message after read
            ref.child(key).delete()

ref.listen(read_messages)
# Function to delete message with specific client ID
# def delete_message(client_id):
#     # ref = db.reference('bars')  # Reference to the 'messages' node in your database
#     messages = ref.get()  # Retrieve all messages
#     if messages:
#         for key, value in messages.items():
#             if value.get('client_id') == client_id:  # Check if client ID matches
#                 ref.child(key).delete()  # Delete the message

# Example usage
# read_messages()  # Read and print all messages
# delete_message("specific_client_id")  # Delete message with specific client ID