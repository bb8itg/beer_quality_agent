import paho.mqtt.client as mqtt

# BROKER_IP = "10.211.55.2"
BROKER_IP = "192.168.2.1"
PORT = 1884
topic = "/barrels/barrel01/ph"

# def parse_barrel_data(topic, message):
    

# Callback function that executes when a message is received
def on_message(client, userdata, message):
    print(message.topic)
    print("Received message:", str(message.payload.decode("utf-8")))

# Create a MQTT client instance
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)

# Define the callback function
client.on_message = on_message

# Connect to the Mosquitto broker running on localhost
try:
    client.connect(BROKER_IP, PORT)
    print('connected')
except ConnectionRefusedError:
    print('Connection error')
# Subscribe to a topic
ph = 4.5
client.publish(topic, ph)
client.disconnect()
# Loop to process messages
# client.loop_forever()