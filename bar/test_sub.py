import paho.mqtt.client as mqtt

BROKER_IP = "10.211.55.2"
PORT = 1884
topic = "/barrels/#"
# /barrels/abc01/ph

# def parse_barrel_data(topic, message):
    

# Callback function that executes when a message is received
def on_message(client, userdata, message):
    print(message.topic)
    print("Received message:", str(message.payload.decode("utf-8")))

# Create a MQTT client instance
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)

# Define the callback function
client.on_message = on_message

# Connect to the Mosquitto broker running on localhost
try:
    client.connect(BROKER_IP, PORT)
    print('connected')
except ConnectionRefusedError:
    print('Connection error')

client.subscribe(topic)
client.loop_start()
try:
    while True:
        pass
except KeyboardInterrupt:
    print("Exiting")
    client.disconnect()
    client.loop_stop() 
# client.loop_forever()