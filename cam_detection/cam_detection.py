# YOLOv5 PyTorch HUB Inference (DetectionModels only)
import torch
import os
import cv2
from firebase import firebase
import json
import numpy as np
import time

FIREBASE_URL = "https://firebass-7a161-default-rtdb.europe-west1.firebasedatabase.app/"

def detect_person(img, model):
    results = model(img)
    data = results.xyxy[0].numpy()
    data = data[data[:,-1].astype(int) == 0, :].astype(int)
    return data

if __name__ == '__main__':
    myfirebase = firebase.FirebaseApplication(FIREBASE_URL, None)
    model = torch.hub.load(os.getcwd(), 'yolov5n', source='local', force_reload=True)
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    
    person_num = 0
    if not cap.isOpened():
        print("Cannot open camera")
        exit()
    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        
        # if frame is read correctly ret is True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        # Our operations on the frame come here
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        data = detect_person(img, model)
        person_num = data.shape[0]
        print(person_num)
        firebase_data = {
        "bar_id": "01",
        "person_num": person_num,
        }
        json_data = json.dumps(firebase_data, indent=4)
        ret = myfirebase.post('bars/person_num', json_data)
        
        # Display the resulting frame
        # for i in range(person_num):
        #     xyxy = data[i, :4]
        #     cv2.rectangle(frame, [xyxy[0],xyxy[1]], [xyxy[2],xyxy[3]], (0,255,0), 2)
        # cv2.putText(frame, str(person_num), (10, h-10), cv2.FONT_HERSHEY_SIMPLEX, 2, (255,0,0), 3)
        # cv2.imshow('frame', frame)
        if cv2.waitKey(1) == ord('q'):
            break
        time.sleep(1)
        
    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
    # im = cv2.imread('pizza/IMG_20240223_120811.jpg')
    # gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    # # im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    # blurred = cv2.GaussianBlur(im, (5, 5), 0)
    # edge_img = cv2.Canny(blurred, 20, 200)
    # canny = cv2.Canny(gray, 50, 200)
    # # cv2.imshow('img', canny)
    # # cv2.waitKey()
    # cv2.imwrite('test.jpg', canny)