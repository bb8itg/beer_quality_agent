#ifdef TEST_SIGNAL_VALE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

// Signal handler function for receiver
void handle_signal(int sig, siginfo_t *siginfo, void *context) {
    float received_data = *((float*)&siginfo->si_value);
    printf("Signal received by receiver.\n");
    printf("Float data received: %.2f\n", received_data);
    exit(0); // Exit after receiving the signal
}

int main() {
    pid_t pid;
    struct sigaction sa;

    // Set up the sigaction struct
    sa.sa_sigaction = handle_signal;
    sa.sa_flags = SA_SIGINFO;

    // Fork a child process
    pid = fork();

    if (pid < 0) {
        fprintf(stderr, "Fork failed\n");
        exit(1);
    } else if (pid == 0) { // Child process (receiver)
        // Register signal handler for receiver
        sigaction(SIGRTMIN, &sa, NULL);

        // Print its process ID
        printf("Receiver Process ID: %d\n", getpid());

        // Wait indefinitely
        while (1) {
            sleep(1);
        }
    } else { // Parent process (sender)
        // Wait a bit to ensure receiver process is ready
        sleep(1);

        // Send the signal to the receiver with additional data
        union sigval value;
        float data_to_send = 3.232322; // Float data to send
        value.sival_int = *(int*)&data_to_send; // Convert float to int
        printf("Sending data to receiver...\n");
        sigqueue(pid, SIGRTMIN, value);
    }

    return 0;
}
#endif