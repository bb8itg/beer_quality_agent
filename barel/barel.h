#ifndef BAREL_H
#define BAREL_H

#include "../globals.h"

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

typedef struct Barel{
    float pHsensor;
    float tempSensor;
    int idBarel;
    struct mosquitto *mosq;
} Barel;

Barel* createBarel(float phVal, float tempVal, int idVal);
void startBarel(Barel* barel);
float readSensorData(Barel* barel);
void deleteBarel(Barel* b);

float readPhData(Barel* barel);

void handle_signal(int sig, siginfo_t *siginfo, void *context);
// ======================== MQTT CALLS ======================
void my_publish_callback(struct mosquitto *mosq, void *obj, int mid);
void my_disconnect_callback(struct mosquitto *mosq, void *obj, int rc);
#endif