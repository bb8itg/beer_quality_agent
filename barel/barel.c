#include "barel.h"
#include <stdlib.h>
#include <time.h>  
 #include <sys/wait.h> // 

static int mid_sent = 0;
static int qos = 0;
static int retain = 1;

volatile float new_pHsensor = -1;
float phData = 4.;
void handle_signal(int sig, siginfo_t *siginfo, void *context) {
    for(int i = 0; i < 15; ++i){
        if (sig == SIGRTMIN+i) {
            float received_data = *((float*)&siginfo->si_value);
            // Extract the new pHsensor value from the signal
            printf("SIGNAL ARRIVED: %i ", i);
            //new_pHsensor = *((float*)(siginfo->si_value.sival_ptr));
            new_pHsensor = received_data;
            printf("Float data received: %.2f\n", received_data);
            phData = new_pHsensor;
        }
    }
}

Barel* createBarel(float phVal, float tempVal, int idVal){
    Barel* returnBarel = (Barel*)malloc(sizeof(Barel));
    returnBarel->pHsensor = phVal;
    returnBarel->tempSensor = tempVal;
    returnBarel->idBarel = idVal;
    returnBarel->mosq = NULL;
    //++idVal;

    mosquitto_lib_init();
    char* idBuffer = (char*)malloc(8);
    snprintf(idBuffer, 8, "%d", returnBarel->idBarel);
    returnBarel->mosq = mosquitto_new(idBuffer, true, NULL);
    if (!returnBarel->mosq)
    {
        printf("Cant initiallize mosquitto library\n");
        exit(-1);
    }
    mosquitto_disconnect_callback_set(returnBarel->mosq, my_disconnect_callback);
    mosquitto_publish_callback_set(returnBarel->mosq, my_publish_callback);
    int rc = mosquitto_connect(returnBarel->mosq, MQTT_HOSTNAME, MQTT_PORT, 60);
    if (rc != 0)
    {
        printf("Client could not connect to broker! Error Code: %d\n", rc);
        mosquitto_destroy(returnBarel->mosq);
        exit(0);
    }
    printf("We are now connected to the broker!\n");

    char* msg_Barel_Data = (char*)malloc(32);
    snprintf(msg_Barel_Data, 32, "init %d", returnBarel->idBarel);
    //mosquitto_publish(returnBarel->mosq, &mid_sent, MQTT_TOPIC, strlen(msg_Barel_Data), msg_Barel_Data, retain, qos);
    /*
        QoS 0: when we prefer that the message will arrive at most once; the message will be received or it won't, there isn't a chance of a duplicate; at most once; fire and forget;
        QoS 1: when we want the message to arrive at least once but don’t care if it arrives twice (or more); at least once;
        QoS 2: when we want the message to arrive exactly once. A higher QOS value means a slower transfer; exactly once.
    */
    printf("we published with mid: %d\n", mid_sent);
    return returnBarel;
}

void startBarel(Barel* barel){


    pid_t pid = fork();

    if (pid == -1) {
        // Error handling for fork failure
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Child process
        // Notify parent about successful sensor data read using signal
        union sigval value;
        printf("\nChild ");
        
        while(1){
            
            time_t t;
            srand((unsigned)time(&t) ^ getpid());
            phData = barel->pHsensor + (float) rand() / (float)(RAND_MAX / 0.5);
            //value.sival_ptr = (void*)&phData;
            value.sival_int = *(int*)&phData; // Convert float to int
            printf("SENDING DATA: %f \n", phData);
            if (sigqueue(getppid(), SIGRTMIN+barel->idBarel, value) == -1) {
                perror("sigqueue failed");
                exit(EXIT_FAILURE);
            }
            sleep(rand()%5 + 1);
        }
    
    } else {
        // Parent process
        // Register signal handler
        struct sigaction act;
        act.sa_sigaction = &handle_signal;
        act.sa_flags = SA_SIGINFO;
        if (sigaction(SIGRTMIN+barel->idBarel, &act, NULL) == -1) {
            perror("sigaction failed");
            exit(EXIT_FAILURE);
        }
        while(1){
            // Wait for the child to terminate
            int status;
            waitpid(pid, &status, 0);

            // Update pHsensor value if new_pHsensor is set
            if (new_pHsensor != -1) {
                barel->pHsensor = new_pHsensor;
                new_pHsensor = -1; // Reset new_pHsensor
            }

            // Publishing MQTT message (assuming mosq is properly initialized)
            char msg_Barel_Data[64];
            snprintf(msg_Barel_Data, 64, "ph %f tmp %f", barel->pHsensor, barel->tempSensor);
            char topicBuffer[64];
            sprintf(topicBuffer, "/barrels/%d", barel->idBarel);
            mosquitto_publish(barel->mosq, NULL, topicBuffer, strlen(msg_Barel_Data), msg_Barel_Data, 0, 0);

        }
    }
}

void deleteBarel(Barel* b){
    mosquitto_destroy(b->mosq);
    mosquitto_lib_cleanup();
    free(b);
}

float readPhData(Barel* barel){
    float phData = barel->pHsensor + (float) rand()/(float)(RAND_MAX/0.5);
    char* msg_Barel_Data = (char*)malloc(32);
   // snprintf(msg_Barel_Data, 32, "id %d ph %f", barel->idBarel, phData);
    snprintf(msg_Barel_Data, 32, "ph %f", phData);
    char topicBuffer[32];
    sprintf(topicBuffer, "/barrels/%d", barel->idBarel);
    mosquitto_publish(barel->mosq, &mid_sent, topicBuffer, strlen(msg_Barel_Data), msg_Barel_Data, retain, qos);
    return phData;
}


#ifndef SENSOR_WITH_SIGNAL

float readSensorData(Barel* barel){
    float phData = barel->pHsensor + (float) rand()/(float)(RAND_MAX/0.5);
    char* msg_Barel_Data = (char*)malloc(64);
   // snprintf(msg_Barel_Data, 32, "id %d ph %f", barel->idBarel, phData);
    snprintf(msg_Barel_Data, 64, "ph %f tmp %f", phData ,barel->tempSensor);
    char topicBuffer[64];
    sprintf(topicBuffer, "/barrels/%d", barel->idBarel);
    mosquitto_publish(barel->mosq, &mid_sent, topicBuffer, strlen(msg_Barel_Data), msg_Barel_Data, retain, qos);
    return phData;
}

#else
float readSensorData(Barel* barel) {
    float phData = barel->pHsensor + (float) rand() / (float)(RAND_MAX / 0.5);

    pid_t pid = fork();

    if (pid == -1) {
        // Error handling for fork failure
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Child process
        // Notify parent about successful sensor data read using signal
        union sigval value;
        value.sival_ptr = (void*)&phData;
        if (sigqueue(getppid(), SIGRTMIN+barel->idBarel, value) == -1) {
            perror("sigqueue failed");
            exit(EXIT_FAILURE);
        }
        exit(EXIT_SUCCESS);
    } else {
        // Parent process
        // Register signal handler
        struct sigaction act;
        act.sa_sigaction = &handle_signal;
        act.sa_flags = SA_SIGINFO;
        if (sigaction(SIGRTMIN+barel->idBarel, &act, NULL) == -1) {
            perror("sigaction failed");
            exit(EXIT_FAILURE);
        }

        // Wait for the child to terminate
        int status;
        waitpid(pid, &status, 0);

        // Update pHsensor value if new_pHsensor is set
        if (new_pHsensor != -1) {
            barel->pHsensor = new_pHsensor;
            new_pHsensor = -1; // Reset new_pHsensor
        }

        // Publishing MQTT message (assuming mosq is properly initialized)
        char msg_Barel_Data[64];
        snprintf(msg_Barel_Data, 64, "ph %f tmp %f", barel->pHsensor, barel->tempSensor);
        char topicBuffer[64];
        sprintf(topicBuffer, "/barrels/%d", barel->idBarel);
        mosquitto_publish(barel->mosq, NULL, topicBuffer, strlen(msg_Barel_Data), msg_Barel_Data, 0, 0);

        return phData;
    }
}
#endif

// ======================== MQTT CALLS ======================
void my_disconnect_callback(struct mosquitto *mosq, void *obj, int rc)
{
    printf("Disconnected!\n");
}

void my_publish_callback(struct mosquitto *mosq, void *obj, int mid)
{
    printf("Published!\n");
}