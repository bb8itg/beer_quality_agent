#include "barel/barel.h"
#include "bar/bar.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>

//gcc test.c bar/*.c barel/*.c -lmosquitto

int main(){
    srand(time(NULL));
    int barelNum = 3;
    Barel* testBarel[barelNum];
    
    for(int i = 0; i < barelNum; i++){
        pid_t pid = fork();
        if( pid == 0){
            testBarel[i] = createBarel(4., 16., i);
            startBarel(testBarel[i]);
            break;
        } else{
            continue;
        }
    }
    /*
    for(int i = 0; i < 20; i++){
        printf("%f\n", readSensorData(testBarel[ rand() % barelNum]));
        sleep(1);
    }
*/
    return 0;
}