#include "barel/barel.h"
#include "bar/bar.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>

//gcc test.c bar/*.c barel/*.c -lmosquitto

int main(){
    Bar* bar = createBar();
    startBar(bar);

    return 0;
}